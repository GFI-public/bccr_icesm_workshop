## Tutorial to work with iCESM water isotope output on NIRD infrastructure



## Description
This project contains the material from the Bjerknes Centre Fast-track initiative focused on introducing water-isotope enabled climate model simulation data to Bjerknes Centre scientists.

Two simulations have been run with iCESM1.2 and iCESM2.0. We provide a fully documented jupyter notebook that allows to easily plot and analyze data from these model simulations.

A more detailed documentation is available in the [project Wiki](https://git.app.uib.no/GFI-public/bccr_icesm_workshop/-/wikis/home)

The project documentation will be detailed further in the future.

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgit.app.uib.no%2FGFI-public%2Fbccr_icesm_workshop/HEAD?labpath=bccr_icesm_tutorial%2FiCESM%2FBCCR_notebook_final.ipynb)

## Visuals

## Installation

## Usage

## Support

## Roadmap

## Contributing

## Authors and acknowledgment

## License
 This project is licensed under the GNU General Public License v3.0 or later. [[ Learn more | https://git.app.uib.no/GFI-public/bccr_icesm_workshop/-/blob/main/LICENSE]]

## Project status
