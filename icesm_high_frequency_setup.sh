#!/bin/sh

#Execute the script in cluster/home/user

#Fill in your username and project number below!
USER="username"
PROJECT="nn9555k"
	
CASE_NAME="test_icesm_high_frequency3"

#echo $PROJECT
##Script stops and  shoots an error message if command fails
#set -e

files_dir=$(pwd)


##GO INTO SCRIPTS
cd /cluster/home/$USER/$PROJECT/progs/iCESM1.2/iCESM1.2/scripts/


##CREATE THE CASE
./create_newcase -case $CASE_NAME -res f19_g16 -compset B1850CN -mach fram


##GO INTO THE CASE
cd $CASE_NAME


##RUN THE SETUP 
./cesm_setup


##COPY DIAGNOSTIC FILE FROM MODEL BASE TO CASE
cp /cluster/home/$USER/$PROJECT/progs/iCESM1.2/iCESM1.2/models/atm/cam/src/physics/cam/cam_diagnostics.F90 SourceMods/src.cam/

##MAKE SOFT LINK TO GET FILES
ln -s /cluster/work/users/hso039/noresm/ncar_reference/ /cluster/home/$USER/$PROJECT/progs/iCESM1.2/iCESM1.2/scripts/$CASE_NAME/work


##GO BACK TO HOME DIRECTORY
cd $files_dir


##MOVE THE MODIFIED FILES TO CASE
cp -i user_nl_cam /cluster/home/$USER/$PROJECT/progs/iCESM1.2/iCESM1.2/scripts/$CASE_NAME
cp -i env_run.xml /cluster/home/$USER/$PROJECT/progs/iCESM1.2/iCESM1.2/scripts/$CASE_NAME
cp -i icesm_high_frequency.run /cluster/home/$USER/$PROJECT/progs/iCESM1.2/iCESM1.2/scripts/$CASE_NAME/$CASE_NAME.run


echo "The case icesm_high_frequency has been set up in scripts"


########DOWNLOAD NECESSARY RESTART FILES

#Move to directory where restart files will be downloaded

#cd /cluster/projects/$PROJECT/work/iCESM1.2

#echo "Start downloading reastart files"

##Downloading list of startup files
#wget -nc https://zenodo.org/record/3553753/files/b.ie12.B1850C5CN.f19_g16.LME.002.cam.h0.1849-12.nc 
#wget -nc https://zenodo.org/record/3553753/files/b.ie12.B1850C5CN.f19_g16.LME.002.cam.h1.1850-01-01-00000.nc
#wget -nc https://zenodo.org/record/3553753/files/b.ie12.B1850C5CN.f19_g16.LME.002.cam.i.1850-01-01-00000.nc
#wget -nc https://zenodo.org/record/3553753/files/b.ie12.B1850C5CN.f19_g16.LME.002.cam.rs.1850-01-01-00000.nc
#wget -nc https://zenodo.org/record/3553753/files/b.ie12.B1850C5CN.f19_g16.LME.002.cice.r.1850-01-01-00000.nc
#wget -nc https://zenodo.org/record/3553753/files/b.ie12.B1850C5CN.f19_g16.LME.002.clm2.h0.1849-12.nc
#wget -nc https://zenodo.org/record/3553753/files/b.ie12.B1850C5CN.f19_g16.LME.002.clm2.h1.1850-01-01-00000.nc
#wget -nc https://zenodo.org/record/3553753/files/b.ie12.B1850C5CN.f19_g16.LME.002.clm2.r.1850-01-01-00000.nc
#wget -nc https://zenodo.org/record/3553753/files/b.ie12.B1850C5CN.f19_g16.LME.002.clm2.rh0.1850-01-01-00000.nc
#wget -nc https://zenodo.org/record/3553753/files/b.ie12.B1850C5CN.f19_g16.LME.002.clm2.rh1.1850-01-01-00000.nc
#wget -nc https://zenodo.org/record/3553753/files/b.ie12.B1850C5CN.f19_g16.LME.002.cpl.r.1850-01-01-00000.nc
#wget -nc https://zenodo.org/record/3553753/files/b.ie12.B1850C5CN.f19_g16.LME.002.pop.r.1850-01-01-00000.nc
#wget -nc https://zenodo.org/record/3553753/files/b.ie12.B1850C5CN.f19_g16.LME.002.pop.ro.1850-01-01-00000
#wget -nc https://zenodo.org/record/3553753/files/b.ie12.B1850C5CN.f19_g16.LME.002.rtm.h0.1849-12.nc
#wget -nc https://zenodo.org/record/3553753/files/b.ie12.B1850C5CN.f19_g16.LME.002.rtm.r.1850-01-01-00000.nc
#wget -nc https://zenodo.org/record/3553753/files/b.ie12.B1850C5CN.f19_g16.LME.002.rtm.rh0.1850-01-01-00000.nc
#wget -nc https://zenodo.org/record/3553753/files/rpointer.atm
#wget -nc https://zenodo.org/record/3553753/files/rpointer.drv
#wget -nc https://zenodo.org/record/3553753/files/rpointer.ice
#wget -nc https://zenodo.org/record/3553753/files/rpointer.lnd
#wget -nc https://zenodo.org/record/3553753/files/rpointer.ocn.ovf
#wget -nc https://zenodo.org/record/3553753/files/rpointer.ocn.restart
#wget -nc https://zenodo.org/record/3553753/files/rpointer.rof
#wget -nc https://zenodo.org/record/3553753/files/surfdata_1.9x2.5_simyr1850_c140303.nc

echo "All restart files downloaded"



echo "Script finished successfully"




