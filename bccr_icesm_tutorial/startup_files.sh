#/bin/sh

#Script stops and  shoots an error message if command fails
set -e


#Execute this script under /cluster/work/users/<username>


#Downloading list of startup files
wget -nc https://zenodo.org/record/3553753/files/b.ie12.B1850C5CN.f19_g16.LME.002.cam.h0.1849-12.nc 
wget -nc https://zenodo.org/record/3553753/files/b.ie12.B1850C5CN.f19_g16.LME.002.cam.h1.1850-01-01-00000.nc
wget -nc https://zenodo.org/record/3553753/files/b.ie12.B1850C5CN.f19_g16.LME.002.cam.i.1850-01-01-00000.nc
wget -nc https://zenodo.org/record/3553753/files/b.ie12.B1850C5CN.f19_g16.LME.002.cam.rs.1850-01-01-00000.nc
wget -nc https://zenodo.org/record/3553753/files/b.ie12.B1850C5CN.f19_g16.LME.002.cice.r.1850-01-01-00000.nc
wget -nc https://zenodo.org/record/3553753/files/b.ie12.B1850C5CN.f19_g16.LME.002.clm2.h0.1849-12.nc
wget -nc https://zenodo.org/record/3553753/files/b.ie12.B1850C5CN.f19_g16.LME.002.clm2.h1.1850-01-01-00000.nc
wget -nc https://zenodo.org/record/3553753/files/b.ie12.B1850C5CN.f19_g16.LME.002.clm2.r.1850-01-01-00000.nc
wget -nc https://zenodo.org/record/3553753/files/b.ie12.B1850C5CN.f19_g16.LME.002.clm2.rh0.1850-01-01-00000.nc
wget -nc https://zenodo.org/record/3553753/files/b.ie12.B1850C5CN.f19_g16.LME.002.clm2.rh1.1850-01-01-00000.nc
wget -nc https://zenodo.org/record/3553753/files/b.ie12.B1850C5CN.f19_g16.LME.002.cpl.r.1850-01-01-00000.nc
wget -nc https://zenodo.org/record/3553753/files/b.ie12.B1850C5CN.f19_g16.LME.002.pop.r.1850-01-01-00000.nc
wget -nc https://zenodo.org/record/3553753/files/b.ie12.B1850C5CN.f19_g16.LME.002.pop.ro.1850-01-01-00000
wget -nc https://zenodo.org/record/3553753/files/b.ie12.B1850C5CN.f19_g16.LME.002.rtm.h0.1849-12.nc
wget -nc https://zenodo.org/record/3553753/files/b.ie12.B1850C5CN.f19_g16.LME.002.rtm.r.1850-01-01-00000.nc
wget -nc https://zenodo.org/record/3553753/files/b.ie12.B1850C5CN.f19_g16.LME.002.rtm.rh0.1850-01-01-00000.nc
wget -nc https://zenodo.org/record/3553753/files/rpointer.atm
wget -nc https://zenodo.org/record/3553753/files/rpointer.drv
wget -nc https://zenodo.org/record/3553753/files/rpointer.ice
wget -nc https://zenodo.org/record/3553753/files/rpointer.lnd
wget -nc https://zenodo.org/record/3553753/files/rpointer.ocn.ovf
wget -nc https://zenodo.org/record/3553753/files/rpointer.ocn.restart
wget -nc https://zenodo.org/record/3553753/files/rpointer.rof
wget -nc https://zenodo.org/record/3553753/files/surfdata_1.9x2.5_simyr1850_c140303.nc


echo "Script finished succesfully"




